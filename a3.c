#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <unistd.h>
#include <pthread.h>

unsigned int memory_size;
char  *memory_p;
char *mfile;
int rqp;
int rsp;
 int map_file;
int mapped_f_size=0;
void write_to(int fd,char *t)
{

int i=0;
	while(t[i]!='\0')
	{
		i++;
	}
    char length=i;
    //printf("The size of %s is %c",t,length);
    write(fd,&length,1);
    //printf("the size is >%d< ",length);
    write(fd,t,length);
}
void write_nr(int fd, unsigned int nr)
{
	write(fd,&nr,sizeof(unsigned int));
}

void create_fifo()
{
	  if((mkfifo("RESP_PIPE_60268", 0666))<0)
	  {
	  	printf("ERROR\ncannot create the response pipe");
	  	exit(-1);
	  }
	  if((rqp=open("REQ_PIPE_60268", O_RDONLY))<0)

	 {
	 	printf("ERROR\ncannot open the request pipe");
	 	 	exit(-1);
	 }




    if((rsp=open("RESP_PIPE_60268", O_WRONLY))<0)
    {
        printf("ERROR\ncannot open the response pipe");
        exit(-1);
    }




}
char *read_instr(int fd)
{
	       char instr;

            read(fd, &instr, 1);

            char *buf =(char*) malloc(instr + 1);
            read(fd, buf, instr);

            buf[(int) instr] = '\0';
            return buf;

}

void garbage()
{
    // int fd=open("test_file",O_RDWR);

    // write_to(fd,"PING");
    // unsigned int nr=60268;
    // write_nr(fd,nr);

    // close(fd);
    // char r;
    // fd=open("test_file",O_RDWR);
    // read(fd,&r,1);
    // read(fd,t,4);
    // t[4]='\0';


//     printf("%s",t);

    //one();
    // int rqp=open("test_file",O_RDWR);

}

void create_shm()
{
	unsigned int size=-1;

			read(rqp,&size,sizeof(unsigned int));
			memory_size=size;
			printf("Need to create a memory with %d",size);
			int memory;
			if((memory=shmget(19659,size,0664))==-1)
			{
                perror("Failed to create");
				write_to(rsp,"CREATE_SHM");
				printf("in eroare1");

				write_to(rsp,"ERROR");
			}
			else
			{
				char *p;

				if((p=(char*)shmat(memory,NULL,0664))==(char*)-1)
				{
					printf("in eroare2");
					write_to(rsp,"CREATE_SHM");
					write_to(rsp,"ERROR");
				}
				else
				{
					write_to(rsp,"CREATE_SHM");
					write_to(rsp,"SUCCESS");
					memory_p=p;
					printf("\nThe memory pointer is %p\n",p);

				}

			}

}
void write_to_shm()
{
unsigned int offset;
			unsigned int value;
			read(rqp,&offset,sizeof(unsigned int));
			read(rqp,&value,sizeof(unsigned int));
			printf("\noff %d value %d and the memory size %d\n",offset,value, memory_size);
			if(offset>=0 && offset<=memory_size&&((offset+sizeof(unsigned int))<=memory_size))
				{



					char *a=memory_p+offset;
					memcpy(a,&value,4);
                    perror("in write");
                    unsigned int test=-2;
                    memcpy(&test,a,4);
                    printf("am citit >%d<",test);
					write_to(rsp,"WRITE_TO_SHM");
					write_to(rsp,"SUCCESS");

				
				}
			else
			{
				write_to(rsp,"WRITE_TO_SHM");
					write_to(rsp,"ERROR");

			}

}
void ping()
{
    write_to(rsp, "PING");
    write_to(rsp, "PONG");
    unsigned int nr = 60268;
    write_nr(rsp, nr);
    printf("Da ping");


}
void map_file_f()
{

			char *t=read_instr(rqp);
            //printf("\nThe files name is %s\n",t);
           map_file=open(t,O_RDWR);
            //printf("am deschis>%d<\n",map_file);
            struct stat sb;
            if (fstat(map_file, &sb) == -1) {
                write_to(rsp, "MAP_FILE");
                write_to(rsp, "ERROR");

            }
            else {
                int size = lseek(map_file,0,SEEK_END);
                lseek(map_file,0,SEEK_SET);
                mapped_f_size=size;
                //printf("\nSizeu ii %d\n",size);
                mfile = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED,map_file, 0);
                if (mfile == MAP_FAILED) {
                    perror("failed map");
                    write_to(rsp, "MAP_FILE");
                    write_to(rsp, "ERROR");

                }
                else
                {
                    write_to(rsp, "MAP_FILE");
                    write_to(rsp, "SUCCESS");


                }
            }
}
void run()
{
	while(1) {

		char *buf=read_instr(rqp);
		if (strcmp("PING", buf) == 0) {
			ping();

		}else if(strcmp("CREATE_SHM",buf)==0)//create shared memory
		{
			create_shm();
		}
		else if(strcmp("WRITE_TO_SHM",buf)==0)//write to memory
		{
			write_to_shm();

		}
		else if(strcmp("MAP_FILE",buf)==0)
		{
			map_file_f();

		}
		else if(strcmp("READ_FROM_FILE_OFFSET",buf)==0)
		{
			unsigned int offset;
			unsigned int nr_bytes;

			read(rqp,&offset,sizeof(unsigned int));
			read(rqp,&nr_bytes,sizeof(unsigned int));
			//printf("\n fd iii %d\n",map_file);
            if(memory_p>0&&mfile>0&&offset>=0 && offset<=memory_size&&((offset+nr_bytes)<=memory_size)&&((offset+nr_bytes)<=mapped_f_size))
            {
			printf("\n%u %u\n",offset,nr_bytes);
            char *src=mfile+offset;
            
            memcpy(memory_p,src,nr_bytes);

            write_to(rsp, "READ_FROM_FILE_OFFSET");
            write_to(rsp, "SUCCESS");
}
else
{
	perror("am ajuns in eroare");
	 write_to(rsp, "READ_FROM_FILE_OFFSET");
            write_to(rsp, "ERROR");
}

		}
		else if(strcmp("READ_FROM_FILE_SECTION",buf)==0)
		{

		}
		else if (strcmp("EXIT", buf) == 0) {
			close(rqp);
            shmdt(memory_p);
			close(rsp);
			break;
		}

	}
	remove("RESP_PIPE_60268");

}
int main()
{
	create_fifo();
	 write_to(rsp,"CONNECT");
	 printf("SUCESS");
	  run();
	// int n=shmget(112,100,664);
	// printf("ready");










	return 0;
}